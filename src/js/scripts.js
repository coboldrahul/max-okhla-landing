jQuery(function($){
  'use-strict'
  $(document).ready(function() {
    
    //-----HeaderPadding
    var headerHeight = $('#header').outerHeight(true);
    // console.log("Header Height", headerHeight);
    // $('main').css('paddingTop', headerHeight);

    //Mobile menu
    $(".MenuBar").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").addClass("Active_Nav");
    });
    $(".MenuOverlay").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").removeClass("Active_Nav");
    });

    function checkScrennSize(){
      if (window.matchMedia('(max-width: 991px)').matches) {
          $(".haveSubmenu>a").append('<span><img src="assets/img/chevron-down-white.svg"></span>');
      }
    }
    checkScrennSize();

    $(".haveSubmenu").find('a>span').click(function(e){
    // $(".haveSubmenu>a>span").click(function(e){
      // e.stopPropagation();
      e.preventDefault();
      $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
    });


    
    $(".contactform").validate({
      rules: {
        Name: "required",
        Email: {
          required: true,
          email: true
        },
        Phone: {
          required: true,
          minlength: 10,
          maxlength: 10
        },
        company: {
          required: true
        }
      },
      messages: {
        Name: "Please enter your name",
        Phone: {
          required: "Please provide phone no.",
          minlength: "Provide a valid phone no."
        },
        Email: "Please enter an email",
        company: "Company field is required"
      },
      submitHandler: function(form) {
        $.ajax({
          url: "mail.php",
          // dataType: "JSON",
          type: "POST",
          data: $(form).serialize(),
          beforeSend: function beforeSend(xhr) {
            $('#submit').val('SENDING...');
          },
          success: function(response) {
            if (response) {
              console.log(response, "Success");
              $('#submit').val('Send Message');
              $('.contactform').append('<div class="success FormMessage">We will get back to you shortly.</div>');
              $(form)[0].reset();
              ga('send', 'event', 'Form', 'Contact Form', 'Contact Form Submission', {
                useBeacon: true
              });
            }
          },
          error: function(err) {
            $('#submit').val('Failed');
            console.log(err, "error");
            $('.contactform').append('<div class="error FormMessage">Unable to submit form. Please try again!</div>');
            $(form)[0].reset();
          }
        });
      }
    });

    $(".mobileform").validate({
      rules: {
        Name: "required",
        Email: {
          required: true,
          email: true
        },
        Phone: {
          required: true,
          minlength: 10,
          maxlength: 10
        },
        company: {
          required: true
        }
      },
      messages: {
        Name: "Please enter your name",
        Phone: {
          required: "Please provide phone no.",
          minlength: "Provide a valid phone no."
        },
        Email: "Please enter an email",
        company: "Company field is required"
      },
      submitHandler: function(form) {
        $.ajax({
          url: "mail.php",
          // dataType: "JSON",
          type: "POST",
          data: $(form).serialize(),
          beforeSend: function beforeSend(xhr) {
            $('#submit').val('SENDING...');
          },
          success: function(response) {
            if (response) {
              console.log(response, "Success");
              $('#submit').val('Send Message');
              $('.mobileform').append('<div class="success FormMessage">We will get back to you shortly.</div>');
              $(form)[0].reset();
              ga('send', 'event', 'Form', 'Contact Form', 'Contact Form Submission', {
                useBeacon: true
              });
            }
          },
          error: function(err) {
            $('#submit').val('Failed');
            console.log(err, "error");
            $('.mobileform').append('<div class="error FormMessage">Unable to submit form. Please try again!</div>');
            $(form)[0].reset();
          }
        });
      }
    });


  }); //--\End document.ready();
});