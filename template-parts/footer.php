
    </main>

    <footer>
        <div class="FooterBottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <ul id="footer-selector">
                            <li>Copyright © <script>document.write(new Date().getFullYear())</script>, Max Okhla House.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul id="additional_links">
                            <li><a href="http://cobold.co/" target="_blank">Delivered by  <strong>Cobold Digital</strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
</div>
<!-- page -->
        

    <script src="assets/js/vendor.js"></script>
    <script src="assets/js/scripts.js"></script>

</body>
</html>