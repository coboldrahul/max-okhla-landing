<section class="HomeBanner">
	<div class="DesktopOnly">
		<picture>
	    	<source srcset="assets/img/tempimg/banner.png" media="(min-width: 768px)">
		    <img src="assets/img/tempimg/mobile.png" alt="" width="100%;">
		</picture>
		<div class="BannerContent">
			<div class="container">
				<div class="BannerContentBlock">
					<h5>Max House Okhla</h5>
					<h1>Open for Business soon! <br>In the heart of New Delhi</h1>
					<h5><a href="tel:+91-9555395222">+91-9555395222</a><span>|</span><a href="mailto:leasing@maxestates.in">leasing@maxestates.in</a></h5>
					<hr>
					<form action="" method="post" class="contactform">
						<div class="row">
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Name" name="Name">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Phone No." name="Phone">
							</div>
							<div class="col-12 col-md-6">
								<input type="email" placeholder="Email" name="Email">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Company Name" name="company">
							</div>
							<div class="col-12 col-md-6">
								<input type="submit" value="Submit" class="" id="submit">
							</div>
							<div class="col-12 col-md-6">
								
							</div>
							<div class="col-12 col-md-6">
								<span><a href="assets/img/max okhla e-brochure.pdf" target="_blank" class="DownloadBtn">Download E-brochure</a></span>
							</div>
						</div>						
					</form>
					
				</div>
			</div>
		</div>
	</div>	
	<div class="MobileOnly">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<picture>
				    	<source srcset="assets/img/tempimg/banner.png" media="(min-width: 768px)">
					    <img src="assets/img/tempimg/mobile.png" alt="" width="100%;">
					</picture>
				</div>
				<div class="col-12 col-md-6">
					<div class="BannerContentBlock">
						<h5>Max House Okhla</h5>
						<h1>Open for Business in the heart of New Delhi</h1>
						<h5><a href="tel:+91-9555395222">+91-9555395222</a><span>|</span><a href="mailto:leasing@maxestates.in">leasing@maxestates.in</a></h5>
						<hr>
						<form action="" method="post" class="mobileform">
							<div class="row">
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Name" name="Name">
								</div>
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Phone No." name="Phone">
								</div>
								<div class="col-12 col-md-6">
									<input type="email" placeholder="Email" name="Email">
								</div>
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Company Name" name="company">
								</div>
								<div class="col-12 col-md-6">
									<input type="submit" value="Submit" class="" id="submit">
								</div>
							</div>						
						</form>
						<span><a href="assets/img/max okhla e-brochure.pdf" target="_blank" class="DownloadBtn">Download E-brochure</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	
</section>