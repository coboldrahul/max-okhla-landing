<section class="PageBannerSection">
	<div class="BannerContainer">
		<div class="BannerOverly"></div>
		<picture class="DesktopOnly">
			<source srcset="assets/img/tempimg/1980x500.png" media="(max-width: 767px)">
			<img src="assets/img/tempimg/1980x500.png" alt="Banner">
		</picture>
		<img src="assets/img/tempimg/500x340.png" ail="Banner" class="MobileOnly">
		<div class="BannerTextConainer">
			<div class="container">
				<div class="BannerText">
					<h1>Contact Us</h1>
					<p>
						<span>
							<a href="#" class="linkColor">Home</a> / <span>Contact Us</span>
						</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>